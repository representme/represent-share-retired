import React, {Component} from 'react';
import { List, ListItem, FontIcon } from 'material-ui';
import FlipMove from 'react-flip-move';
import AlertWarning from 'material-ui/svg-icons/alert/warning';


export default class DuplicateQuestionList extends Component {

  constructor() {
    super();
    this.state = {
      duplicates: []
    }
    this.getDuplicates = this.getDuplicates.bind(this);
  }

  componentWillMount() {
    this.getDuplicates();
  }

  componentWillReceiveProps() {
    if(this.duplicateRequest && this.duplicateRequest.abort) {
      this.duplicateRequest.abort();
    }
    this.getDuplicates();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.search !== this.props.search) {
      return false;
    }
    return true;
  }

  render() {
    return(
      <div>
        <List>
          <FlipMove enterAnimation="fade" leaveAnimation="fade" maintainContainerHeight={true}>
            {this.state.duplicates.map(function(duplicate, index) {
              return (
                  <span onClick={() => window.open('https://represent.me/' + duplicate.id)} key={index}><ListItem primaryText={duplicate.question} leftIcon={<AlertWarning color='#f76f6f' />} /></span>
              );
            })}
          </FlipMove>
        </List>
        {this.state.duplicates.length > 0 && <p style={{color: '#f76f6f'}}>{"Please ensure your question hasn't already been asked"}</p>}
      </div>
    )
  }

  getDuplicates() {
    let search = this.props.search;
    let group = this.props.group || null;

    //Search query must be at least 3 chars long
    if(search.length <= 3) {

      this.setState({
        duplicates: []
      });

    }else {

      this.duplicateRequest = this.props.Represent.API.GETRequest('/api/questions/', {page: 1, page_size: 7, page_size: 3, search, group}, function(result) {
        this.setState({
          duplicates: result.results
        });
      }.bind(this));

    }
  }

}
