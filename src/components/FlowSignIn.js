import React, {Component} from 'react';
import { connect } from 'react-redux';
import Page from './Page';
import { Link } from 'react-router'
import FacebookLogin from './FacebookLogin';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import Autocomplete from 'react-google-autocomplete';
import $ from 'jquery';

class FlowSignIn extends Component {

  constructor(props) {
    super(props);

    this.navigateNext = this.navigateNext.bind(this);
    this.facebookCallback = this.facebookCallback.bind(this);
    this.addExtraProfileInfo = this.addExtraProfileInfo.bind(this);
  }

  render() {
    return (
    	<div className="hasBg">
        <Page style={{height: '100vh', color: '#000000', backgroundColor: '#ffffff', position: 'relative'}} >
          <div className="Question p-a card z-depth-1">

            <div className="card-text p-a text-center">
            <h1>{"Please sign in to continue"}</h1>
              <FacebookLogin
              appId={this.props.Represent.facebookAppId}
              fields="name,email,picture,birthday,gender,hometown"
              callback={this.facebookCallback}
              cssClass="facebook"
              textButton="Connect with Facebook" />
  <br />
              <RaisedButton label="Sign up by email"  onClick={()=>{browserHistory.push("/register" + (this.props.redirect ? '/?redirect=' + encodeURIComponent(this.props.redirect) : '' ))}} />



            <p className="muted small">Already have an account? <Link to={"/signin" + (this.props.redirect ? '/?redirect=' + encodeURIComponent(this.props.redirect) : '' )}>Sign in by email</Link></p>

            </div>
            </div>
            <p className="small aboutNotes muted text-center">Represent is people-first democracy platform which lets you have your say, find and choose the best representatives, take action, and evolve democracy</p>
        </Page>
      </div>
    )
  }

  addExtraProfileInfo(extraInfo) {
    console.log('Making patch request', extraInfo);
    this.props.Represent.API.PATCHRequest('/auth/me/', extraInfo, function(register_result) {
        location.reload()
    }.bind(this), function() {
      this.addExtraProfileInfoFailure();
    }.bind(this));
  }

  addExtraProfileInfoFailure() {
    console.log('FAILED TO COMPLETE USER PROFILE FROM FACEBOOK DATA');
    //browserHistory.push('/set-user-data-fb/'+ (this.props.redirect && '?redirect=' + encodeURIComponent(this.props.redirect)));
    this.navigateNext();
  }

  facebookCallback(data) {

    if(!data.accessToken) {
      //Facebook login failed as it didn't return an accessToken
      return;
    }

    this.props.Represent.API.POSTRequest('/auth-yeti/', {provider: 'facebook', access_token: data.accessToken} , function(result) {
      this.props.Represent.API.authenticateLocalStorage(result.auth_token, function() {

        console.log(data);

        if(!this.props.curUserProfile.address || this.props.curUserProfile.address=='' || !this.props.curUserProfile.dob) {

          console.log('Attempting to update user profile with facebook data');

          if(!data.hometown || !data.email || !data.birthday) { //Check user has properties defined in facebook...
            this.addExtraProfileInfoFailure();
            return;
          }

          let nameParts = data.name.split(' ');
          let formattedDob = null;
          if(data.birthday) {
            formattedDob = data.birthday.split('/');
            formattedDob = formattedDob[2] + '-' + formattedDob[0] + '-' + formattedDob[1];
          }
          let formattedGender = null;
          if(data.gender) {
            switch(data.gender) {
              case 'male':
                formattedGender = 1;
                break;
              case 'female':
                formattedGender = 2;
                break;
              default:
                formattedGender = 3;
            }
          }

          this.profileData = {
            first_name: nameParts[0] || null,
            last_name: nameParts[nameParts.length - 1] || null,
            email: data.email || null,
            gender: formattedGender || null,
            dob: formattedDob,
            address: data.hometown.name || null,
            location: null
          }

          if(data.hometown.name) { //CHECK USER HAS LOCATION, THEN GEOCODE USING GOOGLE TO GET COORDINATES
            this.placesService = new google.maps.places.PlacesService(document.getElementById("headlessGooglePlaces"));
            this.placesService.textSearch({query: data.hometown.name}, function(geocodeResults, geocodeStatus) {
                try {

                  console.log('Geocoding');

                  if (geocodeStatus !== google.maps.places.PlacesServiceStatus.OK) { throw new Error('Google maps places geocode search failed'); } // Throw error if google search fails
                  this.profileData['location'] = {
                      "type": "Point",
                      "coordinates": [geocodeResults[0].geometry.location.lng(), geocodeResults[0].geometry.location.lat()]
                  }
                  this.addExtraProfileInfo(this.profileData);
                } catch(e) {
                  console.log(e);
                  this.addExtraProfileInfoFailure();
                }
            }.bind(this));
          }else {
            this.addExtraProfileInfoFailure();
          }

        } else {
          this.navigateNext();
        }
      }.bind(this));
    }.bind(this));

  }

  navigateNext() {
    browserHistory.push(decodeURIComponent(this.props.redirect));
  }

}
const mapStateToProps = function(store) {
  return {
    curUserProfile: store.userState.curUserProfile
  };
};
export default connect(mapStateToProps)(FlowSignIn);
