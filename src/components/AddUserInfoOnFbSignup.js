import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Page } from './';
import { browserHistory } from 'react-router'

import Autocomplete from 'react-google-autocomplete';
import * as EmailValidator from 'email-validator';

import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import Avatar from 'material-ui/Avatar';
import TextField from 'material-ui/TextField';

import './styles/Register.css';

class AddUserInfoOnFbSignup extends Component {

    constructor(props) {
        super(props);

        this.state = {
            initUsername: this.props.curUserProfile.username,
            initEmail: this.props.curUserProfile.email,
            formValues: {
                email: this.props.curUserProfile.email,
                // password: '',
                username: this.props.curUserProfile.username,
                first_name: this.props.curUserProfile.first_name,
                last_name: this.props.curUserProfile.last_name,
                gender: false,
                dobDay: null,
                dobMonth: null,
                dobYear: null,
                location: null,
            },
            formErrors: {
                email: null,
                // password: null,
                username: null
            },
            error: null,
            reqInProcess: false
        };

        this.setValue = this.setValue.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeFirstName = this.onChangeFirstName.bind(this);
        this.onChangeLastName = this.onChangeLastName.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onChangeGender = this.onChangeGender.bind(this);
        this.onChangeDobDay = this.onChangeDobDay.bind(this);
        this.onChangeDobMonth = this.onChangeDobMonth.bind(this);
        this.onChangeDobYear = this.onChangeDobYear.bind(this);
        this.onChangeLocation = this.onChangeLocation.bind(this);

        this.checkEmail = this.checkEmail.bind(this);
        this.checkUsername = this.checkUsername.bind(this);
        // this.checkPassword = this.checkPassword.bind(this);

        this.saveInfo = this.saveInfo.bind(this);

        this.dobYears = [];
        for (var i = 2013; i > 1900; i--) {
            this.dobYears.push(i);
        }

        this.locationInputStyle = {
            'WebkitAppearance': 'textfield',
            'WebkitTapHighlightColor': 'rgba(0, 0, 0, 0)',
            'width': '100%',
            'border': 'none',
            'outline': 'none',
            'backgroundColor': 'rgba(0, 0, 0, 0)',
            'color': 'rgba(0, 0, 0, 0.870588)',
            'position': 'relative',
            'padding': '0px',
            'height': '100%',
            'fontSize' : '16px',
            'boxSizing': 'border-box',
            'marginTop': '30px',
            'cursor': 'inherit',
            'borderBottom' : '1px solid #ddd',
            'paddingBottom' : '4px',
        };
    }

    render() {
        return (
          <div className="RegisterForm hasBg">
              <Page>
                  <div className="hasBg">
                      <div className="Question p-a card z-depth-1">
                          <div className="card-text p-a" style={{'minHeight': '200px'}}>
                              <span>Please complete your profile</span>

                              {/*

                              <div className="avatar-container">
                                  <Avatar
                                    src={this.props.curUserProfile.photo}
                                    size={100}
                                    className="avatar-img"
                                />
                              </div>

                              */}

                              <TextField
                                value={this.state.formValues.email}
                                floatingLabelText="Your email"
                                onChange={this.onChangeEmail}
                                onBlur={this.checkEmail}
                                errorText={this.state.formErrors.email}
                                className="textInput fullWidth"
                              />

                              {/*

                              <TextField
                                value={this.state.formValues.first_name}
                                floatingLabelText="First name"
                                onChange={this.onChangeFirstName}
                                className="textInput fullWidth"
                              />

                              <TextField
                                value={this.state.formValues.last_name}
                                floatingLabelText="Last name"
                                onChange={this.onChangeLastName}
                                className="textInput fullWidth"
                              />

                              <TextField
                                value={this.state.formValues.username}
                                floatingLabelText="Username"
                                onChange={this.onChangeUsername}
                                onBlur={this.checkUsername}
                                errorText={this.state.formErrors.username}
                                className="textInput fullWidth"
                              />

                              */}

                              <SelectField
                                value={this.state.formValues.gender}
                                maxHeight={200}
                                onChange={this.onChangeGender}>
                                  <MenuItem value={false} primaryText="Gender" disabled={true} />
                                  <MenuItem value={1} primaryText="Man" />
                                  <MenuItem value={2} primaryText="Woman" />
                                  <MenuItem value={3} primaryText="Other" />
                                  <MenuItem value={0} primaryText="Rather not say" />
                              </SelectField>

                              <div className="dob">
                                  <SelectField
                                    floatingLabelText="Day"
                                    value={this.state.formValues.dobDay}
                                    onChange={this.onChangeDobDay}
                                    className="textInput dobSelect"
                                    autoWidth={true}
                                  >
                                      <MenuItem value={1} primaryText="01" />
                                      <MenuItem value={2} primaryText="02" />
                                      <MenuItem value={3} primaryText="03" />
                                      <MenuItem value={4} primaryText="04" />
                                      <MenuItem value={5} primaryText="05" />
                                      <MenuItem value={6} primaryText="06" />
                                      <MenuItem value={7} primaryText="07" />
                                      <MenuItem value={8} primaryText="08" />
                                      <MenuItem value={9} primaryText="09" />
                                      <MenuItem value={10} primaryText="10" />
                                      <MenuItem value={11} primaryText="11" />
                                      <MenuItem value={12} primaryText="12" />
                                      <MenuItem value={13} primaryText="13" />
                                      <MenuItem value={14} primaryText="14" />
                                      <MenuItem value={15} primaryText="15" />
                                      <MenuItem value={16} primaryText="16" />
                                      <MenuItem value={17} primaryText="17" />
                                      <MenuItem value={18} primaryText="18" />
                                      <MenuItem value={19} primaryText="19" />
                                      <MenuItem value={20} primaryText="20" />
                                      <MenuItem value={21} primaryText="21" />
                                      <MenuItem value={22} primaryText="22" />
                                      <MenuItem value={23} primaryText="23" />
                                      <MenuItem value={24} primaryText="24" />
                                      <MenuItem value={25} primaryText="25" />
                                      <MenuItem value={26} primaryText="26" />
                                      <MenuItem value={27} primaryText="27" />
                                      <MenuItem value={28} primaryText="28" />
                                      <MenuItem value={29} primaryText="29" />
                                      <MenuItem value={30} primaryText="30" />
                                      <MenuItem value={31} primaryText="31" />
                                  </SelectField>
                                  <SelectField
                                    floatingLabelText="Month"
                                    value={this.state.formValues.dobMonth}
                                    onChange={this.onChangeDobMonth}
                                    className="textInput dobSelect"
                                    autoWidth={true}
                                  >
                                      <MenuItem value={1} primaryText="January" />
                                      <MenuItem value={2} primaryText="February" />
                                      <MenuItem value={3} primaryText="March" />
                                      <MenuItem value={4} primaryText="April" />
                                      <MenuItem value={5} primaryText="May" />
                                      <MenuItem value={6} primaryText="June" />
                                      <MenuItem value={7} primaryText="July" />
                                      <MenuItem value={8} primaryText="August" />
                                      <MenuItem value={9} primaryText="September" />
                                      <MenuItem value={10} primaryText="October" />
                                      <MenuItem value={11} primaryText="November" />
                                      <MenuItem value={12} primaryText="December" />
                                  </SelectField>
                                  <SelectField
                                    floatingLabelText="Year"
                                    value={this.state.formValues.dobYear}
                                    onChange={this.onChangeDobYear}
                                    className="textInput dobSelect"
                                    autoWidth={true}
                                  >
                                      {this.dobYears.map(function(year) {
                                          return (<MenuItem key={year} value={year} primaryText={year} />)
                                      })}
                                  </SelectField>
                              </div>

                              <Autocomplete
                                style={this.locationInputStyle}
                                onPlaceSelected={this.onChangeLocation}
                                types={['(regions)']}
                              />
                              {this.state.error && (
                                <div style={{color: 'red'}}>{this.state.error}</div>
                              )}
                              <button onClick={!this.state.reqInProcess && this.saveInfo} className="m-y">
                                  {this.state.reqInProcess ? 'Adding..' : 'Save'}
                              </button>
                          </div>
                      </div>
                  </div>
              </Page>
          </div>
        );
    }


    checkEmail(e) {
        let email = e.target.value;
        let isEmailValid = EmailValidator.validate(email);
        if(!isEmailValid) {
            this.setError({
                email: 'This is not a valid email'
            });
        } else if(email !== this.state.initEmail) {
            this.props.route.Represent.API.GETRequest('/auth/check_email/', {email: email}, function (check_result) {
                let isInUse = check_result.result === true;
                this.setError({
                    email: isInUse ? 'This email is already in use' : null
                });
            }.bind(this));
        } else {
            this.setError({
                email: null
            });
        }
    }

    checkUsername(e) {
        let username = e.target.value;
        if(username.indexOf(' ') != -1) {
            this.setError({
                username: 'Username can not contain spaces'
            });
        } else if(username !== this.state.initUsername){
            this.props.route.Represent.API.GETRequest('/api/users/', {username__iexact: username}, function (check_result) {
                let isInUse = check_result.results.length > 0;
                this.setError({
                    username: isInUse ? 'This username is already in use' : null
                });
            }.bind(this))
        } else {
            this.setError({
                username: null
            });
        }
    }

    // checkPassword(e) {
    //     let pass = e.target.value;
    //     this.setError({
    //         password: pass.length >= 8 ? null : 'Password should be at least 8 character long'
    //     })
    // }

    onChangeEmail(e) {
        let email = e.target.value;
        let valueObj = {
            email: email
        };
        // let atIndex = email.indexOf('@');
        // valueObj['username'] = email.substr(0, atIndex == -1 ? email.length : atIndex);

        this.setValue(valueObj)
    }

    onChangeFirstName(e) {
        this.setValue({
            first_name:  e.target.value
        })
    }

    onChangeLastName(e) {
        this.setValue({
            last_name:  e.target.value
        })
    }

    onChangeUsername(e) {
        this.setValue({
            username: e.target.value.replace(' ', '_')
        })
    }

    onChangeGender(e, i, value) {
        this.setValue({
            gender: value
        })
    }

    onChangeDobDay(e, i, v) {
        this.setValue({
            dobDay: v
        })
    }
    onChangeDobMonth(e, i, v) {
        this.setValue({
            dobMonth: v
        })
    }
    onChangeDobYear(e, i, v) {
        this.setValue({
            dobYear: v
        })
    }
    onChangeLocation(place) {
        this.setValue({
            location: place
        })
    }
    setValue(errorObj) {
        this.setState(function (prevState) {
            return {
                formValues: Object.assign({}, prevState.formValues, errorObj),
                error: null
            }
        });
    }

    setError(errorObj) {
        this.setState(function (prevState) {
            return {
                formErrors: Object.assign({}, prevState.formErrors, errorObj)
            }
        });
    }

    saveInfo() {
        this.setState({
            reqInProcess: false
        });
        let validationError = null;

/*

        FULL VALIDATION: (Now using reduced fields)

        if(!EmailValidator.validate(this.state.formValues.email)) {
            validationError = 'Please enter a valid email address';
        } else if(!this.state.formValues.first_name) {
            validationError = 'Please type in first name';
        } else if(!this.state.formValues.last_name) {
            validationError = 'Please type in last name';
        } else if(!this.state.formValues.username) {
            validationError = 'Please type in username';
        } else if(this.state.formValues.gender === false) {
            validationError = 'Please select your gender';
        } else if(!this.state.formValues.location) {
            validationError = 'Please type in your postcode';
        } else if(!this.state.formValues.dobDay || !this.state.formValues.dobMonth || !this.state.formValues.dobYear) {
            validationError = 'Please select your date of birth';
        }

*/

        if(!EmailValidator.validate(this.state.formValues.email)) {
            validationError = 'Please enter a valid email address';
        } else if(this.state.formValues.gender === false) {
            validationError = 'Please select your gender';
        } else if(!this.state.formValues.location) {
            validationError = 'Please type in your postcode';
        } else if(!this.state.formValues.dobDay || !this.state.formValues.dobMonth || !this.state.formValues.dobYear) {
            validationError = 'Please select your date of birth';
        }

        if(validationError) {
            this.setState({error: validationError});
        } else {
            this.setState({
                reqInProcess: true
            });
            let reqObj = {
                email: this.state.formValues.email,
                /*
                first_name: this.state.formValues.first_name,
                last_name: this.state.formValues.last_name,
                */
                username: this.state.formValues.username,
                gender: this.state.formValues.gender,
                dob: this.state.formValues.dobYear + "-" + this.state.formValues.dobMonth + "-" + this.state.formValues.dobDay,

            };
            let locLat = this.state.formValues.location['geometry']['location'].lat();
            let locLon = this.state.formValues.location['geometry']['location'].lng();
            if (!!locLat) {
                reqObj['location'] = {
                    "type": "Point",
                    "coordinates": [locLon, locLat]
                };
            }
            reqObj['address'] = this.state.formValues.location.formatted_address;

            this.props.route.Represent.API.PATCHRequest('/auth/me/', reqObj, function(register_result) {
                browserHistory.push(this.props.route.Represent.findGetParameter('redirect'));
            }.bind(this));
        }
    }
}
const mapStateToProps = function(store) {
    return {
        curUserProfile: store.userState.curUserProfile
    };
};
export default connect(mapStateToProps)(AddUserInfoOnFbSignup);
