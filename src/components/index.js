import Page from './Page.js';
import Flow from './Flow';
import Notification from './Notification';
import FacebookLogin from './FacebookLogin';
import DuplicateQuestionList from './DuplicateQuestionList';
import SearchTags from './SearchTags';
import FlowSignIn from './FlowSignIn';

module.exports = {
  Page,
  Flow,
  Notification,
  FacebookLogin,
  DuplicateQuestionList,
  SearchTags,
  FlowSignIn
}
