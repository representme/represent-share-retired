import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import { Page } from './components';
import * as EmailValidator from 'email-validator';
import { browserHistory } from 'react-router';
import Autocomplete from 'react-google-autocomplete';
import RaisedButton from 'material-ui/RaisedButton';

import './components/styles/Register.css';

class Register extends Component {

  constructor() {
    super();

    this.state = {
      showingMode: 'signup', // or 'addFbInfo'
      formValues: {
        email: '',
        password: '',
        username: '',
        dobDay: null,
        dobMonth: null,
        dobYear: null,
        gender: false,
        location: null,
        tou: false,
      },
      formErrors: {
        email: null,
        password: null,
        username: null
      },
      error: null
    };

    this.register = this.register.bind(this);
    this.facebookCallback = this.facebookCallback.bind(this);
    this.navigateNext = this.navigateNext.bind(this);
    this.registerErrorObjToStr = this.registerErrorObjToStr.bind(this);

    this.setError = this.setError.bind(this);
    this.setValue = this.setValue.bind(this);

    this.onChangeEmail = this.onChangeEmail.bind(this);
    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onChangeDobDay = this.onChangeDobDay.bind(this);
    this.onChangeDobMonth = this.onChangeDobMonth.bind(this);
    this.onChangeDobYear = this.onChangeDobYear.bind(this);
    this.onChangeGender = this.onChangeGender.bind(this);
    this.onChangeLocation = this.onChangeLocation.bind(this);
    this.onChangeTou = this.onChangeTou.bind(this);

    this.checkEmail = this.checkEmail.bind(this);
    this.checkUsername = this.checkUsername.bind(this);
    this.checkPassword = this.checkPassword.bind(this);


    // just copied from browser to apply to google autocompete component
    this.locationInputStyle = {
      'WebkitAppearance': 'textfield',
      'WebkitTapHighlightColor': 'rgba(0, 0, 0, 0)',
      'width': '100%',
      'border': 'none',
      'outline': 'none',
      'backgroundColor': 'rgba(0, 0, 0, 0)',
      'color': 'rgba(0, 0, 0, 0.870588)',
      'position': 'relative',
      'padding': '0px',
      'height': '100%',
      'fontSize' : '16px',
      'boxSizing': 'border-box',
      'marginTop': '30px',
      'cursor': 'inherit',
      'borderBottom' : '1px solid #ddd',
      'paddingBottom' : '4px',
    };

    this.dobYears = [];
    for (var i = 2013; i > 1900; i--) {
      this.dobYears.push(i);
    }
  }

  render() {
    return (
      <div className="RegisterForm hasBg">
        <Page>

          <div className="hasBg">
            {this.state.showingMode == 'addFbInfo' && (<div className="Question p-a card z-depth-1">
              <AddUserInfoOnFbSignup API={this.props.route.Represent.API} navigateNext={this.navigateNext} />
            </div>)}
            {this.state.showingMode == 'signup' && (<div className="Question p-a card z-depth-1">

              <div className="card-action">


                <TextField
                  value={this.state.formValues.email}
                  floatingLabelText="Your email"
                  onChange={this.onChangeEmail}
                  onBlur={this.checkEmail}
                  errorText={this.state.formErrors.email}
                  className="textInput fullWidth"
                />


                <Autocomplete
                  style={this.locationInputStyle}
                  onPlaceSelected={this.onChangeLocation}
                  types={['(regions)']}
                />

                <TextField
                  value={this.state.formValues.username}
                  floatingLabelText="Username"
                  onChange={this.onChangeUsername}
                  onBlur={this.checkUsername}
                  errorText={this.state.formErrors.username}
                  className="textInput fullWidth"
                />
                <TextField
                  value={this.state.formValues.password}
                  floatingLabelText="Set a password"
                  onChange={this.onChangePassword}
                  onBlur={this.checkPassword}
                  errorText={this.state.formErrors.password}
                  type="password"
                  className="textInput fullWidth"
                />
                <div className="dob">
                  <SelectField
                      floatingLabelText="Day"
                      value={this.state.formValues.dobDay}
                      onChange={this.onChangeDobDay}
                      className="textInput dobSelect"
                      autoWidth={true}
                  >
                    <MenuItem value={1} primaryText="01" />
                    <MenuItem value={2} primaryText="02" />
                    <MenuItem value={3} primaryText="03" />
                    <MenuItem value={4} primaryText="04" />
                    <MenuItem value={5} primaryText="05" />
                    <MenuItem value={6} primaryText="06" />
                    <MenuItem value={7} primaryText="07" />
                    <MenuItem value={8} primaryText="08" />
                    <MenuItem value={9} primaryText="09" />
                    <MenuItem value={10} primaryText="10" />
                    <MenuItem value={11} primaryText="11" />
                    <MenuItem value={12} primaryText="12" />
                    <MenuItem value={13} primaryText="13" />
                    <MenuItem value={14} primaryText="14" />
                    <MenuItem value={15} primaryText="15" />
                    <MenuItem value={16} primaryText="16" />
                    <MenuItem value={17} primaryText="17" />
                    <MenuItem value={18} primaryText="18" />
                    <MenuItem value={19} primaryText="19" />
                    <MenuItem value={20} primaryText="20" />
                    <MenuItem value={21} primaryText="21" />
                    <MenuItem value={22} primaryText="22" />
                    <MenuItem value={23} primaryText="23" />
                    <MenuItem value={24} primaryText="24" />
                    <MenuItem value={25} primaryText="25" />
                    <MenuItem value={26} primaryText="26" />
                    <MenuItem value={27} primaryText="27" />
                    <MenuItem value={28} primaryText="28" />
                    <MenuItem value={29} primaryText="29" />
                    <MenuItem value={30} primaryText="30" />
                    <MenuItem value={31} primaryText="31" />
                  </SelectField>
                  <SelectField
                      floatingLabelText="Month"
                      value={this.state.formValues.dobMonth}
                      onChange={this.onChangeDobMonth}
                      className="textInput dobSelect"
                      autoWidth={true}
                  >
                    <MenuItem value={1} primaryText="January" />
                    <MenuItem value={2} primaryText="February" />
                    <MenuItem value={3} primaryText="March" />
                    <MenuItem value={4} primaryText="April" />
                    <MenuItem value={5} primaryText="May" />
                    <MenuItem value={6} primaryText="June" />
                    <MenuItem value={7} primaryText="July" />
                    <MenuItem value={8} primaryText="August" />
                    <MenuItem value={9} primaryText="September" />
                    <MenuItem value={10} primaryText="October" />
                    <MenuItem value={11} primaryText="November" />
                    <MenuItem value={12} primaryText="December" />
                  </SelectField>
                  <SelectField
                      floatingLabelText="Year"
                      value={this.state.formValues.dobYear}
                      onChange={this.onChangeDobYear}
                      className="textInput dobSelect"
                      autoWidth={true}
                  >
                    {this.dobYears.map(function(year) {
                      return (<MenuItem key={year} value={year} primaryText={year} />)
                    })}
                  </SelectField>
                </div>
                {/*<DatePicker*/}
                  {/*value={this.state.formValues.dob}*/}
                  {/*onChange={this.onChangeDob}*/}
                  {/*maxDate={this.state.maxDate}*/}
                  {/*hintText="What's your date of birth?"*/}
                  {/*locale="en_GB"*/}
                {/*/>*/}

                <SelectField
                  value={this.state.formValues.gender}
                  maxHeight={200}
                  onChange={this.onChangeGender}>
                  <MenuItem value={false} primaryText="Gender" disabled={true} />
                  <MenuItem value={1} primaryText="Man" />
                  <MenuItem value={2} primaryText="Woman" />
                  <MenuItem value={3} primaryText="Other" />
                  <MenuItem value={0} primaryText="Rather not say" />
                </SelectField>



                <div className="TOC">
                  <label><Checkbox
                    value={this.state.formValues.tou}
                    className="Checkboxed"
                    onCheck={this.onChangeTou}
                  />
                  <span className="CheckboxedLabel">I have read and agree to the <a href="http://help.represent.me/policies/terms-of-use/">terms and conditions</a> and <a href="http://help.represent.me/policies/privacy-policy/">privacy policy</a></span></label>
                </div>




    <RaisedButton label="Join" primary={true}  onClick={this.register} style={{marginRight: '15px', backgroundColor: '#1B8AAE'}} />
    <RaisedButton label="Cancel"  onClick={this.navigateNext} />

              </div>

              {this.state.error && (
                <div style={{color: 'red'}}>{this.state.error}</div>
              )}

            </div>)}
          </div>
          <p className="small aboutNotes muted text-center">Represent is people-first democracy platform which lets you have your say, find and choose the best representatives, take action, and evolve democracy</p>

        </Page>
      </div>
    )
  }


  checkEmail(e) {
    let email = e.target.value;
    let isEmailValid = EmailValidator.validate(email);
    if(!isEmailValid) {
      this.setError({
        email: 'This is not a valid email'
      });
    } else {
      this.props.route.Represent.API.GETRequest('/auth/check_email/', {email: email}, function (check_result) {
        let isInUse = check_result.result === true;
        this.setError({
          email: isInUse ? 'This email is already in use' : null
        });
      }.bind(this))
    }
  }

  checkUsername(e) {
    let username = e.target.value;
    if(username.indexOf(' ') != -1) {
      this.setError({
        username: 'Username can not contain spaces'
      });
    } else {
      this.props.route.Represent.API.GETRequest('/api/users/', {username__iexact: username}, function (check_result) {
        let isInUse = check_result.results.length > 0;
        this.setError({
          username: isInUse ? 'This username is already in use' : null
        });
      }.bind(this))
    }
  }
  checkPassword(e) {
    let pass = e.target.value;
    this.setError({
      password: pass.length >= 8 ? null : 'Password should be at least 8 character long'
    })
  }

  onChangeEmail(e) {
    let email = e.target.value;
    let valueObj = {
      email: email
    };
    let atIndex = email.indexOf('@');
    valueObj['username'] = email.substr(0, atIndex == -1 ? email.length : atIndex);

    this.setValue(valueObj)
  }
  onChangeUsername(e) {
    this.setValue({
      username: e.target.value.replace(' ', '_')
    })
  }
  onChangePassword(e) {
    this.setValue({
      password: e.target.value
    });
    if(this.state.formErrors.password) {
      this.checkPassword(e);
    }
  }
  onChangeDobDay(e, i, v) {
    this.setValue({
      dobDay: v
    })
  }
  onChangeDobMonth(e, i, v) {
    this.setValue({
      dobMonth: v
    })
  }
  onChangeDobYear(e, i, v) {
    this.setValue({
      dobYear: v
    })
  }
  onChangeGender(e, i, value) {
    this.setValue({
      gender: value
    })
  }
  onChangeLocation(place) {
    this.setValue({
      location: place
    })
  }
  onChangeTou(e, checked) {
    this.setValue({
      tou: checked
    })
  }

  setValue(errorObj) {
    this.setState(function (prevState) {
      return {
        formValues: Object.assign({}, prevState.formValues, errorObj)
      }
    });
  }

  setError(errorObj) {
    this.setState(function (prevState) {
      return {
        formErrors: Object.assign({}, prevState.formErrors, errorObj)
      }
    });
  }

  getErrorMessage(errorObj) {
    let errorMessage = null;
    if(errorObj['username']) {
      errorMessage = 'Too long! 30 characters maximum!';
    } else {
      errorMessage = JSON.stringify(errorObj);
    }
    return errorMessage;
  }

  facebookCallback(data) {

    if(!data.accessToken) {
      //Facebook login failed
      return;
    }

    this.props.route.Represent.API.POSTRequest('/auth-yeti/', {provider: 'facebook', access_token: data.accessToken} , function(result) {
      this.props.route.Represent.API.authenticateLocalStorage(result.auth_token, function() {
        this.setState({
          showingMode: 'addFbInfo'
        })
      }.bind(this));
    }.bind(this));
  }

  registerErrorObjToStr(error, errorObj) {
    let errorMessage = null;
    if(errorObj) {
      if(errorObj['username']) {
        errorMessage = 'Too long! 30 characters maximum!';
      }
    } else {
      errorMessage = error
    }

    return errorMessage;
  }

  register() {
    let validationError = null;

    if(!EmailValidator.validate(this.state.formValues.email)) {
      validationError = 'Please enter a valid email address';
    } else if(!this.state.formValues.location) {
      validationError = 'Please type in your postcode';
    } else if(!this.state.formValues.username) {
      validationError = 'Please type in username';
    } else if(!this.state.formValues.password || this.state.formValues.password.length < 8) {
      validationError = 'Password is less than 8 characters';
    } else if(!this.state.formValues.dobDay || !this.state.formValues.dobMonth || !this.state.formValues.dobYear) {
      validationError = 'Please select your date of birth';
    } else if(this.state.formValues.gender === false) {
      validationError = 'Please select your gender';
    } else if(!this.state.formValues.tou) {
      validationError = 'You must agree to the terms and conditions';
    }

    // Check if account already exists

    if(validationError) {
      this.setState({error: validationError});
    } else {
      this.setState({error: null});

      let reqObj = {
        email: this.state.formValues.email,
        username: this.state.formValues.username,
        password: this.state.formValues.password,
        gender: this.state.formValues.gender,
        dob: this.state.formValues.dobYear + "-" + this.state.formValues.dobMonth + "-" + this.state.formValues.dobDay,
      };
      let locLat = this.state.formValues.location['geometry']['location'].lat();
      let locLon = this.state.formValues.location['geometry']['location'].lng();
      if (!!locLat) {
        reqObj['location'] = {
          "type": "Point",
          "coordinates": [locLon, locLat]
        };
      }
      reqObj['address'] = this.state.formValues.location.formatted_address;


      // Register
      this.props.route.Represent.API.POSTRequest('/auth/register/', reqObj, function(register_result) {

        console.log('Signed up successfully');
        this.props.route.Represent.API.authenticateLocalStorage(register_result.auth_token, function() {
          this.navigateNext();
        }.bind(this));

      }.bind(this), function(register_result) {

        this.setState({
          errorObj: this.getErrorMessage(register_result.responseJSON)
        });

      }.bind(this));


    }
  }

  navigateNext() {
    if(this.props.route.Represent.findGetParameter('redirect')) {
      console.log('Going to ' + this.props.route.Represent.findGetParameter('redirect'));
      browserHistory.push(this.props.route.Represent.findGetParameter('redirect'));
    }else {
      browserHistory.push('/');
    }
  }

}

export default Register;
