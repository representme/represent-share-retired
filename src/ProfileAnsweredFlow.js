import React, {Component} from 'react';
import store from './store';
import { connect } from 'react-redux';
import {Flow} from './components';

class ProfileAnsweredFlow extends Component {

  constructor() {
    super();
    this.state = {
      questions: [],
      noQuestions: null,
      unansweredCountOnFirstLoad: null,
      flowData: null
    };
    this.loadedQuestionsIdsStr = '';

    this.onVoteChange = this.onVoteChange.bind(this);
    this.loadNextQuestions = this.loadNextQuestions.bind(this);
  }

  componentWillMount() {

    // Request for the questions

    this.loadNextQuestions();

    // End of request

    this.props.route.Represent.API.GETRequest('/api/users/' + this.props.params.profileid + '/', null, function(profile_response) {
      let userName = profile_response.first_name ? profile_response.first_name + profile_response.last_name : profile_response.username;
      this.setState({
        flowData: {
          type: 'profile',
          title: userName,
          image: profile_response.photo
        }
      });
      store.dispatch({
        type: 'setCurrentFlowData',
        flowType: 'profile',
        title: userName,
        image: profile_response.photo,
        id: profile_response.id,
        slug: profile_response.username
      })
    }.bind(this));

  }

  componentWillReceiveProps(nextProps) {
    if(!this.flowStateDataLoadStarted && (this.props.curUserProfile || (!this.props.curUserProfile && nextProps.curUserProfile))) {
      this.flowStateDataLoadStarted = true;
      this.props.route.Represent.API.GETRequest('/api/next_question/', {
            answered_by: parseInt(this.props.params.profileid, 10),
          },
          function (response) {
            let answeredCount = response.count - response.unanswered;
            store.dispatch({
              type: 'setPercentageCompletedInCurrentFlow',
              percentageCompleted: response.count != 0 ? parseInt((answeredCount / response.count) * 100) : 0,
              questionsCount: response.count,
              answeredCount: answeredCount
            });
          }.bind(this)
      );
    }
  }

  render() {
    return (
      <div style={{height: '100vh'}}>
        <Flow questions={this.state.questions} flowData={this.state.flowData} Represent={this.props.route.Represent} loadNextQuestions={this.loadNextQuestions} onVoteChange={this.onVoteChange} noQuestions={this.state.noQuestions} unansweredCountOnFirstLoad={this.state.unansweredCountOnFirstLoad} />
      </div>
    );
  }

  loadNextQuestions() {
    this.props.route.Represent.API.GETRequest('/api/next_question/', {
        answered_by: parseInt(this.props.params.profileid, 10),
        limit_count: 2,
        'id__in!': (this.loadedQuestionsIdsStr != '' ? this.loadedQuestionsIdsStr : null)
      },
      function(response) {
        this.setState(function(curState) {
          return {
            questions: curState.questions.concat(response.results),
            noQuestions: response.unanswered == 0 || response.returning == response.unanswered,
            unansweredCountOnFirstLoad: response.unanswered
          }
        });

        let arr = [];
        for (var i = 0; i < response.results.length; i++) {
          arr.push(response.results[i].id);
        }
        this.loadedQuestionsIdsStr += (this.loadedQuestionsIdsStr.length==0 ? '': ',')+arr.join(',');
      }.bind(this),
      function(err) {
        if(err.status == 401) {
          this.props.route.Represent.API.logout(function(){
            window.location.reload();
          })
        }
      }.bind(this)

    );
  }

  onVoteChange(id, newvote) {
    let questions = this.state.questions;
    for( var i = 0, len = questions.length; i < len; i++ ) {
        if( questions[i]['id'] === id) {

          this.props.route.Represent.API.GETRequest('/api/questions/', {id: id}, function(response) {
              questions[i] = response.results[0];
              this.setState({
                questions: questions
              });
            }.bind(this),
          );

          break;

        }
    }
  }

}

const mapStateToProps = function(store) {
  return {
    curUserProfile: store.userState.curUserProfile,
    currentFlowState: store.currentFlowState
  };
};
export default connect(mapStateToProps)(ProfileAnsweredFlow);
