import axios from 'axios';
import store from './store';
import cookie from 'react-cookie';

class RepresentAPI {

  constructor() {

    this.auth_token = null;
    this.apiurl = '';

    // Detirmine which URL to use

    if (location.host === 'represent.me' || location.host === 'share.represent.me') {
      this.apiurl = 'https://api.represent.me';
      this.cookieurl = '';
    } else if (location.host === 'share-test.represent.me' || location.host === 'test.represent.me') { // test
      this.apiurl = 'https://test.represent.me';
      this.cookieurl = '';
    } else if (location.host === '10.0.2.2:3000') { // virtual box
      this.apiurl = 'http://10.0.2.2:8000';
      this.cookieurl = '';
    } else { // local
      this.apiurl = 'http://localhost:8000';
      this.cookieurl = '';
    }

    // Detirmine auth token override

    let auth_override = this.findGetParameter('auth_override');

    if(auth_override) {
      this.authenticateLocalStorage(auth_override);
    }

    // Detirmine whether user is logged in or not

    if(this.auth_token === null) {

      // let local_token = localStorage.getItem('auth_token');
      let local_token = cookie.load('auth_token');

      if(local_token === null) {
        this.auth_token = null;
      }else {
        this.auth_token = local_token;
      }
    }

    if(this.auth_token) {
      console.log('Using auth token ' + this.auth_token);
    }else {
      console.log('Not using auth token');
    }

  }

  authenticated() {
    if(this.auth_token) {
      return true;
    }else {
      return false;
    }

  }

  GETRequest(url, data, success, failure) {

    axios.get(this.apiurl + url, {
      dataType: "json",
      headers: {
        'Authorization': this.auth_token ? "Token " + this.auth_token : null
      },
      params: data
    }).then(function(output) {
      success(output.data);
    }).catch(function(output) {
      if(failure) {
        failure(output);
      }else {
        console.log('GET ' + url + ' failed', output);
      }
    })

  }

  POSTRequest(request, data, success, failure) {

    axios.post(this.apiurl + request, data, {
      dataType: "json",
      contentType: "application/json",
      headers: {
        'Authorization': this.auth_token ? "Token " + this.auth_token : null
      }
    }).then(function(output) {
      success(output.data);
    }).catch(function(output) {
      if(failure) {
        failure(output);
      }else {
        console.log('POST ' + request + ' failed');
      }
    });

  }

  PATCHRequest(request, data, success, failure) {

    axios.patch(this.apiurl + request, data, {
      dataType: "json",
      contentType: "application/json",
      headers: {
        'Authorization': this.auth_token ? "Token " + this.auth_token : null
      }
    }).then(function(output) {
      success(output.data);
    }).catch(function(output) {
      if(failure) {
        failure(output);
      }else {
        console.log('PATCH ' + request + ' failed');
      }
    });

  }

  authenticateLocalStorage(auth_token, callback) {
    this.auth_token = auth_token;
    this.GETRequest('/auth/me/', null, function(user) {

      trackJs.configure({
        userId: user.username
      });

      trackJs.addMetadata("name", user.first_name + ' ' + user.last_name);

      trackJs.console.log("User logged in");

      store.dispatch({
        type: 'userLoggedIn',
        userProfile: user
      });
      window.analytics.identify(user.username, user);
      // localStorage.setItem('auth_token', this.auth_token);
      // localStorage.setItem('user', JSON.stringify(user));
      let curDate = new Date();
      let expires = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate()+2);
      cookie.save('auth_token', this.auth_token, { path: '/', expires: expires, domain: this.cookieurl });
      let newUser = {
        first_name: user.first_name,
        last_name: user.last_name,
        dob: user.dob,
        id: user.id,
        username: user.username,
        email: user.email,
        photo: user.photo,
        address: user.address,
      };
      cookie.save('user', JSON.stringify(newUser), { path: '/', expires: expires, domain: this.cookieurl });
      if(callback) {
        callback();
      }
    }.bind(this));
  }

  // for some reason JQuery thinks that this request fails, but it has success status, so error function is used here for processing response
  // probably it is because /auth/logout/ returns nothing
  logout(callback) {
    this.POSTRequest('/auth/logout/', null, function(){
      store.dispatch({
        type: 'userLoggedOut'
      });
      // localStorage.removeItem('auth_token');
      // localStorage.removeItem('user');
      cookie.remove('auth_token', { path: '/', domain: this.cookieurl });
      cookie.remove('user', { path: '/', domain: this.cookieurl });
      if(callback) {
        callback();
      }
    }.bind(this));
  }

  findGetParameter(parameterName) {
    //Helper function to retrieve GET values from the URL
    var result = null,
      tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
      tmp = items[index].split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
  }

}

export default RepresentAPI;
